const express = require('express')
const path = require('path')

const app = express()

const PORT = process.env.PORT || 3000

app.use(express.static('public'))

app.get('/test', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/views/test.html'))
})

app.get('/',(req, res) => {
    res.sendFile(path.join(__dirname, 'public/views/index.html'))
})

app.listen(PORT, () => {
    console.log(`Server live on port ${PORT}`)
})