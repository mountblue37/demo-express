const increase = document.querySelector('#increase')
const decrease = document.querySelector('#decrease')
const num = document.querySelector('#num')
let number = 0

increase.addEventListener('click', (event) => {
    number++
    num.innerText = number
})

decrease.addEventListener('click', (event) => {
    number--
    num.innerText = number
})